//
//  ViewController.swift
//  CameraAppSample
//
//  Created by ADMIN on 27/01/21.
//

import UIKit
import NetworkExtension
import CoreLocation

class ViewController: UIViewController,UINavigationControllerDelegate {
    
    struct HeartBeatData {
        var txVersionData: NSData?
        let txVersionLength = 4
    }
    
    var navHeight: CGFloat = 44.0
    var hartbeatTime: Int32 = 5
    var runCount = 0
    
    var WEPEnabledPassword : String = "admin"
    
    var queueSetup = DataQueueSetup.init()
    var heartbeatData = HeartBeatData()
    
    var sdk: iRearCam2SDK?
    
    var displayView: SNDisplayView2!
    
    var errorMsgLbl:UILabel?
    var lineImgView: UIImageView!

    
    
    private let dataQueue = DispatchQueue(label: "com.data.queueLock")
    private let videoDataCondition = NSCondition.init()
    private var isForeground = true
    private var isFirstRun = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CameraAppSample"
       
        setupCameraView()
        
        initialiseCamera()
      
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if UIDevice.current.orientation.isLandscape
        {
            DispatchQueue.main.async {
                self.displayView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-50)
//                self.errorMsgLbl = UILabel.init(frame: CGRect.init(x: 300, y:self.displayView.frame.size.height+20, width: self.view.frame.width*0.6, height: 30))
                self.errorMsgLbl?.frame = CGRect(x: self.view.frame.size.width/2 - 50, y: self.displayView.frame.size.height+20, width: self.view.frame.width*0.6, height: 30)
                self.lineImgView.frame = CGRect.init(x: 0, y: 0.0, width: self.displayView.frame.width, height: self.displayView.frame.height)
                print("landscapeLeft or landscapeRight")
            }
           
        }
        else{
            var navHeight: CGFloat = 0.0
            if #available(iOS 11.0, *) {
                navHeight = view.safeAreaInsets.top
            } else {
                navHeight = (navigationController?.navigationBar.frame.height)!
            }
            DispatchQueue.main.async {
                self.displayView.frame = CGRect.init(x: 0, y: navHeight, width: self.view.frame.width, height: self.view.frame.height/16*9)
                self.errorMsgLbl?.frame = CGRect(x: self.view.frame.size.width/2 - 50, y: self.displayView.frame.size.height+20, width: self.view.frame.width*0.6, height: 30)
                self.lineImgView.frame = CGRect.init(x: 0, y: 0.0, width: self.displayView.frame.width, height: self.displayView.frame.height)
            print("portrait")
            }
        }
    }
    
    func setupCameraView()
    {
        let isSoftWareDecode = false
        //A12 & A12X use software decode
        
        displayView = SNDisplayView2.init(frame: CGRect.init(x: 0, y: navHeight, width: view.frame.width, height: view.frame.width/16*9), withSWDecode: isSoftWareDecode);//y value need to make constant
        view.addSubview(displayView!)
        lineImgView = UIImageView.init(image: UIImage.init(named: "NoPreview"))
        lineImgView.isHidden = true
        lineImgView.contentMode = .scaleAspectFit
        lineImgView.frame = CGRect.init(x: 0.0, y: 0.0, width: displayView.frame.width, height: displayView.frame.height)
        displayView.layer.addSublayer(lineImgView.layer)
        
        errorMsgLbl = UILabel.init(frame: CGRect.init(x: self.view.frame.width/2 - 50, y:displayView.frame.size.height+50, width: view.frame.width*0.6, height: 30))
        view.addSubview(errorMsgLbl!)
        errorMsgLbl?.isHidden = true
    }
    
    func initialiseCamera() {
        if self.sdk == nil {
           
            queueSetup.startThread(target: self, selector: #selector(self.handleVideoQueues(_:)), object: nil)
        }
        self.sdk = iRearCam2SDK.init(sdkWithDelegate: self)
        self.sdk?.setHearbeatTimeout(hartbeatTime, microseconds: 0)
        self.sdk?.setWEPEnable(true, withPassword: WEPEnabledPassword)
        self.sdk?.createIRearCam2()
        if let res = self.sdk?.createHBSocket(), res {
            self.sdk?.startHeartBeat()
            print("Start HB")
        }
        else {
            print("Failed create HB")
        }
        //To do should be call below 2 methods even after heart beat failure also ?
        self.sdk?.sendStartCmd()
        self.sdk?.sendSyncCmd()
        print("iRearCam2SDK Library Start")
    }
    
    @objc func handleVideoQueues(_ sender: Thread) {
        
        while !queueSetup.isThreadCanceled() {
            self.videoDataCondition.lock()
            let queueObj = queueSetup.getData()
            if let queueData = queueObj, isForeground {
                displayView?.receivedRawVideoFrame(queueData.bytes, withSize: queueData.size)
                queueSetup.remove(at: 0)
            } else {
                self.videoDataCondition.wait()
            }
            self.videoDataCondition.unlock()
        }
        queueSetup.cancelThread()
    }
    
    //MARK: - Foreground/Background Handle
    func applicationDidBecomeActive() {//Background to foreground
        isForeground = true
        displayView?.rebuildSampleBufferDisplayLayer()
        initialiseCamera()
    }
    func applicationWillResignActive() {// foreground to background
        isForeground = false
        print("sdk:", sdk)
        shutdownCamera()
    }
    
    func shutdownCamera() {
        
        if let sdk_ = self.sdk {
            print("closeHandle: sdk not nil")
            sdk?.delegate = nil
            sdk_.sendStopCmd()
            sdk_.stopStartCmd()
            sdk_.stopSyncCmd()
            sdk_.closeHeartBeat()
            sdk_.releaseIRearCam2()
                
            sdk = nil
        }
        self.videoDataCondition.lock()
        queueSetup.releaseAll()
        self.videoDataCondition.signal()
        self.videoDataCondition.unlock()
    }
}

//MARK: - iRearCam2SDKDelegate

extension ViewController: iRearCam2SDKDelegate {
    
    //The event for not receiving the data of heartbeat
    //@param error error message
    func didFail(toReceiveHeartBeatData error: Error?) {
        print(error.debugDescription)
        
        if isForeground {
            shutdownCamera()
            DispatchQueue.main.async { [self] in
                initialiseCamera()
                errorMsgLbl?.isHidden = false
                //TODO crashing
                lineImgView?.isHidden = false
                        if let errorString = error?.localizedDescription{
                            errorMsgLbl?.text = errorString
                        }
                    }
            }
        }
    
    
    //Receive heartbeat data
    //@param data Heartbeat data
    func didReceiveHeartBeat(_ data: Data?) {
        
        if let data = data {
            //Get Tx Version
            let txRange = Range.init(NSRange.init(location: 0, length: heartbeatData.txVersionLength))
            if let txRange = txRange {
                heartbeatData.txVersionData = data.subdata(in: txRange) as NSData
            }
            
        }
    }
    
    //Receive Video data
    //@param bytes Video data
    //@param size The size of video data
    //@param time timestampdele
    //@param isIFrame if YES, current frame is i-frame
    func receiveVideoData(_ bytes: UnsafeMutablePointer<UInt8>?, size: UInt64, timestamp time: TimeInterval, iFrame isIFrame: Bool) {
       
        self.videoDataCondition.lock()
        if let bytes = bytes, isForeground {
            queueSetup.appendData(bytes: bytes, size: size, time: time)
        }
        self.videoDataCondition.signal()
        self.videoDataCondition.unlock()
    }
}


