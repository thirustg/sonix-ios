//
//  DataObject.swift
//  iRearCam2
//
//  Created by Wally on 2019/6/20.
//  Copyright © 2019 snx. All rights reserved.
//

import UIKit

struct DataObject {
    var data: Data?
    var bytes: UnsafeMutablePointer<UInt8>?
    var size: UInt64
    var time: TimeInterval
    init() {
        data = nil
        bytes = nil
        size = 0
        time = 0.0
    }
}

//class DataObject2 {
//    var data: Data?
//    var bytes: UnsafeMutablePointer<UInt8>?
//    var size: UInt64
//    var time: TimeInterval
//    init(count: Int) {
//        data = nil
//        bytes = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(count))
//        size = UInt64(count)
//        time = 0.0
//    }
//    deinit {
//        bytes?.deinitialize(count: Int(size))
//    }
//}
