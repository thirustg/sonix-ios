//
//  DataQueueSetup.swift
//  iRearCam2
//
//  Created by Wally on 2019/6/20.
//  Copyright © 2019 snx. All rights reserved.
//

import UIKit

class DataQueueSetup: NSObject {
    
    private var queueThread: Thread?
    private var queueData = Array<DataObject>()
    var bufferingValue = 5
    var dataLock = NSLock.init()
    
    //MARK: - Get Class Parameter
    
    func queueLength() -> Int {
        return queueData.count
    }
    
    func isThreadCanceled() -> Bool {
        var result = true
        if let thread = queueThread {
            result = thread.isCancelled
        }
        return result
    }
    
    //MARK: - Queue Thread
    func startThread(target: Any, selector: Selector, object argument: Any?) {
        if queueThread == nil {
            print("run once")
            queueThread = Thread.init(target: target, selector: selector, object: argument)
            queueThread?.start()
        }
        
    }
    
    func cancelThread() {
        if let thread = queueThread {
            thread.cancel()
            queueThread = nil
        }
    }
    
    //MARK: - Queue Array
    func appendData(bytes: UnsafeMutablePointer<UInt8>, size: UInt64, time: TimeInterval) {
        dataLock.lock()
        var object = DataObject()
        object.bytes = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(size))
        object.bytes?.initialize(from: bytes, count: Int(size))
        object.size = size
        object.time = time
        queueData.append(object)
        dataLock.unlock()
    }
    
    func appendData(data: Data, time: TimeInterval) {
        
        dataLock.lock()
        var object = DataObject()
        object.data = data
        object.size = UInt64(data.count)
        object.time = time
        queueData.append(object)
        dataLock.unlock()

//        var dataCopy = Data(data)
//        let dataCount = dataCopy.count
//        dataCopy.withUnsafeMutableBytes({ (bytes: UnsafeMutablePointer<UInt8>) -> Void in
//            //Use `bytes` inside this closure
//            var object = DataObject()
//            object.bytes = bytes
//            object.size = UInt64(dataCount)
//            object.time = time
//            queueData.append(object)
//            dataLock.unlock()
//        })
        
    }
    
    func getData() -> DataObject? {
        var data: DataObject?
        dataLock.lock()
        if queueData.count > 0 {
            data = queueData[0]
        }
        dataLock.unlock()
        return data
    }
    
    func remove(at index: Int) {
        
        dataLock.lock()
        if queueData.count > 0 {
            var aData = queueData[0]
            if let bytes = aData.bytes {
                bytes.deallocate()
            }
            aData.data = nil
            queueData.remove(at: index)
        }
        dataLock.unlock()
    }
    
    func removeAllQueue() {
        
        dataLock.lock()
        for aObject in queueData {
            if let bytes = aObject.bytes {
                bytes.deallocate()
            }
        }
        dataLock.unlock()
    }
    
    //MARK: - Class Release
    
    func releaseAll() {
        if let thread = queueThread {
            thread.cancel()
            queueThread = nil
        }
        dataLock.lock()
        for aObject in queueData {
            if let bytes = aObject.bytes {
                bytes.deallocate()
            }
        }
        queueData.removeAll()
        dataLock.unlock()
    }
    
}
