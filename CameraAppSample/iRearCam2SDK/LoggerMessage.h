//
//  LoggerMessage.h
//  iRearCam2
//
//  Created by sonix on 2019/6/25.
//  Copyright © 2019 snx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoggerMessage : NSObject
@property (nonatomic, getter=isWritten) BOOL write;
- (void)redirectNSlogToDocumentFolder;
- (void)closeNSlogToDocumentFolder;

@end

NS_ASSUME_NONNULL_END
