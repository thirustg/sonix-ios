//
//  SNAudioRecorder.h
//  iRearCam2SDK
//
//  Created by Wally on 2020/11/11.
//  Copyright © 2020 snx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioUnit/AudioUnit.h>


NS_ASSUME_NONNULL_BEGIN

@protocol SNAudioRecorderDelegate <NSObject>

- (void)receiveRecordData:(NSData *)data;

@end

@interface SNAudioRecorder : NSObject

@property (nonatomic, weak) id <SNAudioRecorderDelegate> _Nullable delegate;
@property (nonatomic, assign) BOOL isRecording;

- (instancetype)initWithSampleRate:(Float64)sampleRate;

- (OSStatus)startRecord;
- (OSStatus)stopRecord;

@end

NS_ASSUME_NONNULL_END
