//
//  SNAudioPlayer.h
//  SNSDK
//
//  Created by yeh chinwei on 2015/10/7.
//  Copyright (c) 2015年 SNX. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>

@interface SNAudioPlayer : NSObject

@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, getter=isStartPushTalk) BOOL startPushTalk;


- (OSStatus)startPlay;
- (OSStatus)stopPlay;

- (instancetype)initWithSampleRate:(Float64)sampleRate;

- (void)enqueueData:(NSData *)data;
- (void)clearQueue;
@end
