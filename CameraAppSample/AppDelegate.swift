//
//  AppDelegate.swift
//  iRearCam2
//
//  Created by Wally on 2019/6/17.
//  Copyright © 2019 snx. All rights reserved.
//

import UIKit
//import DropDown
import CoreTelephony

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let homeVC = ViewController()
    var isFirstRun = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 10, *) {
            networkStatus()
        }
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let navVC = UINavigationController.init(rootViewController: homeVC)
//        navVC.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = navVC
        window?.makeKeyAndVisible()
       // DropDown.startListeningToKeyboard()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
       
        homeVC.applicationWillResignActive()
        print("applicationWillResignActive finished")//1
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if !isFirstRun {
            isFirstRun = true
        } else {
            homeVC.applicationDidBecomeActive()
        }
        print("applicationDidBecomeActive finished")//2
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func networkStatus() {
        let cellularData = CTCellularData.init()
        cellularData.cellularDataRestrictionDidUpdateNotifier = { state in
            
            switch state {
            case .restricted:
                print("restricted")
                break
            case .notRestricted:
                print("notRestricted")
                break
            case .restrictedStateUnknown:
                print("restrictedStateUnknown")
                break
            default:
                break
            }
            
        }
        
    }


}

extension UIApplication {
    class func visiableViewController(rootViewController: UIViewController?) -> UIViewController? {
        if let vc_ = rootViewController?.presentedViewController {
            if vc_.isKind(of: UINavigationController.self) {
                let nav = vc_ as! UINavigationController
                if let lastVC = nav.viewControllers.last {
                    return visiableViewController(rootViewController: lastVC)
                }
            }
        }
        return rootViewController
    }
}
