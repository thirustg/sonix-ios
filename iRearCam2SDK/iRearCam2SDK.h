//
//  iRearCam2SDK.h
//  iRearCam2SDK
//
//  Created by Wally on 2019/6/17.
//  Copyright © 2019 snx. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol iRearCam2SDKDelegate <NSObject>

@optional

/// Receive video data
/// @param bytes Video data
/// @param size The size of video data
/// @param time timestamp
/// @param isIFrame If YES, current frame is i-frame
- (void)receiveVideoData:(uint8_t *_Nullable)bytes size:(uint64_t)size timestamp:(NSTimeInterval)time iFrame:(BOOL)isIFrame;

/// Receive device's opcode and data
/// @param code Opcode
/// @param data data
- (void)didReceiveDeviceStausCode:(int)code withData:(nullable NSData *)data;

/// Receive heartbeat data.
/// @param data Heartbeat data
- (void)didReceiveHeartBeatData:(nullable NSData *)data;


/// The event for not receiving the data of heattbeat
/// @param error error message
- (void)didFailToReceiveHeartBeatData:(nullable NSError *)error;

/// Receive percentage when firmware is upgrading
/// @param current The size of current data to send
/// @param total The size of firmware file
- (void)didReceiveFWUpgradePercentageWithLength:(int)current totalLength:(int)total;


/// Receive audio data
/// @param bytes Audio data
/// @param size The size of audio data
/// @param time timestamp
- (void)receiveAudioData:(uint8_t *_Nullable)bytes size:(uint64_t)size timestamp:(NSTimeInterval)time;

@end

enum {
    iRearCam2SDKCode_TWC_CMD_OK = 0x00,
    iRearCam2SDKCode_No_Resource_Connect = 0x01,
    iRearCam2SDKCode_VDO_Certificate_Error = 0x02,
    
    iRearCam2SDKCode_CRCFrameOK = 0x07,
    iRearCam2SDKCode_FWUpdateOK = 0x15,
    iRearCam2SDKCode_FWUpdateFailed = 0x16,
    
    iRearCam2SDKCode_Mirror_Flip = 0x30,
    iRearCam2SDKCode_Brightness = 0x80,
    iRearCam2SDKCode_Chroma = 0x81,
    iRearCam2SDKCode_Contract = 0x82,
    iRearCam2SDKCode_Saturation = 0x83,
    
    iRearCam2SDKCode_Switch_Mode = 0x8e,
    iRearCam2SDKCode_Audio_Volume_CTL = 0x8C,
    iRearCam2SDKCode_PushTalk_Status = 0x8D,
} iRearCam2SDKCode;


@interface iRearCam2SDK : NSObject
@property (nonatomic, assign) id <iRearCam2SDKDelegate> _Nullable delegate;


/// Init SDK Library
/// @param delegate delegate
- (instancetype _Nullable)initSDKWithDelegate:(id _Nullable)delegate;



/// Default value is set to YES. Use to drop frame.
/// @param filter flag
- (void)setupVideoFilter:(BOOL)filter;


/// Set up the device's WEP password. The password length must be at least 5 lengths and no more than 13.
/// @param enable YES, enable the WEP password.
/// @param pass If enabled, set up the password.
- (BOOL)setWEPEnable:(BOOL)enable withPassword:(NSString * _Nullable)pass;

/// Init parameters and generate network sockets
/**
    Describe how to start receive video
 
    Usage Example:
    @code
    // Step1:
    sdk_.createIRearCam2()
    // Step2:
    sdk_.sendStartCmd()
    // Step3:
    sdk_.sendSyncCmd()
    @endcode

*/
- (void)createIRearCam2;

/// Release parameters and close sockets
/**
    Describe how to stop and release video
 
    Usage Example:
    @code
    // Step1:
    sdk_.sendStopCmd()
    // Step2:
    sdk_.stopStartCmd()
    // Step3:
    sdk_.stopSyncCmd()
    // Step4:
    sdk_.uninit()
    @endcode
*/
- (void)releaseIRearCam2;


/// Get video's bitrate
- (int32_t)getBitrate;

/// Set heartbeat timeout. Default value set to 5 seconds.
/// @param sec The time in seconds.
/// @param usec The time in microseconds.
- (void)setHearbeatTimeout:(int)sec microseconds:(int)usec;


/// Create heartbeat socket. Return Yes, success; otherwise, socket create failed.
- (BOOL)createHBSocket;

/// Start send and receive hearbeat data
- (void)startHeartBeat;

/// Close heartbeat
- (void)closeHeartBeat;


- (void)sendStartCmd;
- (void)sendStopCmd;
- (void)stopStartCmd;
- (void)sendSyncCmd;
- (void)stopSyncCmd;


/// Call this method when the firmware is upgrade failed or interrupt upgrading
/// @param isInterrupt Set value 1 to intterupt firmware upgrade
- (void)interruptFWUpgrade:(int)isInterrupt;

/// Start upgrade firmware
/// @param data Data of firmware file
- (void)startFirmwareUpgrade:(NSData *_Nullable)data;

/// Set data by using opcode, Ex: set image flip, image btightness, etc.
/// @param opCode opcode value
/// @param data The value which you would like to change.
/**
    Define:
    @code
    //Image define
    enum DeviceCommand: Int {
        case MIRROR_FLIP = 0x30
        case BRIGHTNESS = 0x80      // Range 0~255, default(128)
        case CHROMA = 0x81          // Range 0~255, default(128)
        case CONTRACT = 0x82        // Range 0~255, default(128)
        case SATURATION = 0x83      // Range 0~255, default(128)
    }
 
    //Image flip define
    enum ImageFlip: Int {
        case NO_CHANGE = 0
        case FLIP = 1
        case MIRROR = 2
        case MIRROR_FLIP = 3
    }
    @endcode
 */
/**
    Describe how to set data with opcode
 
    Usage Example:
    @code
    //Set brightness value
    let data = Data.init(bytes: [UInt8(128)])
    sdk?.setDataWithOPCode(UInt8(DeviceCommand.BRIGHTNESS.rawValue), data: data)
    //Set image mirror
    let opcode = UInt8(DeviceCommand.MIRROR_FLIP.rawValue)
    let selection = UInt8(ImageFlip.MIRROR.rawValue)
    let data = Data.init(bytes: [selection, 0, 0])
    sdk?.setDataWithOPCode(opcode, data: data)
    @endcode
*/
- (void)setDataWithOPCode:(Byte)opCode data:(NSData *_Nullable)data;

/// Receive the data of opcode from didReceiveDeviceStausCode:withData: method
/// @param opCode opcode
- (void)getDataWithOPCode:(Byte)opCode;


/// Send audio data to the device
/// @param data Audio data
- (void)sendPushTalkWithData:(NSData *_Nonnull)data;

@end
