//
//  SNAudio32.h
//  iRearCam2SDK
//
//  Created by Wally on 2020/11/9.
//  Copyright © 2020 snx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SNAudio32 : NSObject

/*-------------------------Encoder-------------------------------------*/

/// Init encoder of Audio32. Current support sample rate 16kHz.
- (BOOL)audio32EncodeInit;

/// Encode audio32 with pcm data. The length of the data must be a multiple of 640.
/// @param data Raw data of pcm.
- (NSData *  _Nullable)encode:(NSData *)data;
/*---------------------------------------------------------------------*/


/*-------------------------Decoder-------------------------------------*/

/// Init decoder of Audio32. Current support sample rate 16kHz.
- (BOOL)audio32DecodeInit;

/// Decode audio32. The length of the data must be a multiple of 40.
/// @param data Audio32 data
- (NSData * _Nullable)decode:(NSData *)data;
/*---------------------------------------------------------------------*/
@end

NS_ASSUME_NONNULL_END
