//
//  SNDisplayView2.h
//  SNSDK
//
//  Created by yeh chinwei on 2015/10/28.
//  Copyright © 2015年 SNX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol SNDisplayView2Delegate <NSObject>


/**
 Obtain the image data after decode finished. Use setUpDecodeFormat: method to change the format. Default format is kCVPixelFormatType_32BGRA.

 @param imageBuffer image data.
 */
- (void)didReceiveDecodeFinishedWithImageBuffer:(CVPixelBufferRef)imageBuffer;
@end

@interface SNDisplayView2 : UIView

@property (assign, nonatomic) id <SNDisplayView2Delegate> delegate;

@property (nonatomic) int outputWidth, outputHeight;

@property (nonatomic, strong) NSData *currentSPS;
@property (nonatomic, strong) NSData *currentPPS;



- (BOOL)receivedRawVideoFrame:(uint8_t *)frame withSize:(uint64_t)frameSize;
- (void)flushBuffer;
- (void)resetParameterSet;
- (void)rebuildSampleBufferDisplayLayer;


/**
 Initialize with third party decode.

 @param frame frame size
 @param status YES, use third party to decode
 @return self
 */
- (id)initWithFrame:(CGRect)frame withSWDecode:(BOOL)status;

/**
 You should close the hardware drawing if you need to custom your own view.
 The didReceiveDecodeImageFinished: method will be called if you set the status to NO.

 @param status Default is YES. Set NO to close the hardware drawing.
 */
- (void)setUpHardwareDraw:(BOOL)status;


/**
 Setup decode image format. Default format is kCVPixelFormatType_32BGRA. See more formats in Apple's document. (https://developer.apple.com/documentation/corevideo/1563591-pixel_format_identifiers?language=objc)

 @param format Decode format.
 */
- (void)setUpDecodeFormat:(NSInteger)format;




@end
